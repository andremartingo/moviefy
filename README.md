<p align="center">
  <img width="150" height="150" src="https://img.deusm.com/informationweek/2015/09/1322066/Swift_logo.png" />
</p>

<a href="https://developer.apple.com/swift/" target="_blank">
  <img src="https://img.shields.io/badge/Swift-4.0-orange.svg?style=flat" alt="Swift 4.1">
</a>

# Moviefy

Here is a fully functional and colorful iOS app which I made from scratch. <p> App follow MVVM-C architecture.
<p> Taking the best from Reactive Programming for binding View Controller and View Model.

# Features

With the app, you can:

  - Discover the most popular, the most rated or the highest rated movies
  - Search for movies
 
## Screenshots

<img width="250" src="./Screenshots/popular.png"/> <img width="250" src="./Screenshots/details.png"/>

<img width="250" src="./Screenshots/search.png"/>


## Buzzwords

* MVVM-C
* ReactiveSwift
* ReactiveCocoa
* URLSession
* Chaining Asynchronous Requests
* API
* UIKit


## Instalation

```bash
git clone https://gitlab.com/andremartingo/moviefy.git
open Moviefy.xcodeproj
```


## Run Unit Tests

```bash
open Moviefy.xcodeproj
CMD + U
```
