import Foundation
import XCTest
import ReactiveSwift
@testable import Moviefy


class SearchViewModelTestCase: XCTestCase {
    
    private var viewModel: SearchViewModel!
    private var store: MockStore!
    
    
    // MARK: - Setup
    
    override func setUp() {
        super.setUp()
        
        store = MockStore()
        viewModel = SearchViewModel(store: store)
    }
    
    override func tearDown() {
        
        viewModel = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testSearch_WhenSearch_ShouldHaveTwoCells() {
        let firstMovie = TMDBMovie(title: "movie1", id: 1)
        let secondMovie = TMDBMovie(title: "movie2", id: 1)
        
        let dataExpectation = expectation(description: "dataExpectation")
        
        store.mockSearchMovies = .success([firstMovie,secondMovie])
        
        viewModel.dataViewModels.signal
            .observeValues {
                XCTAssertEqual($0.count, 2)
                dataExpectation.fulfill()
        }
        
        viewModel.searchTerm.value = "venom"
        
        waitForExpectations(timeout: 1.0)
    }
    
    func testSearch_WhenTapMovie_ShouldGoToDetails() {
        let firstMovie = TMDBMovie(title: "movie1", id: 1)
        let secondMovie = TMDBMovie(title: "movie2", id: 1)

        let navigationExpectation = expectation(description: "dataExpectation")

        store.mockSearchMovies = .success([firstMovie,secondMovie])

        viewModel.navigation
            .observeValues {
                switch $0 {
                case .showMovieDetails(_,_): break
                }
                navigationExpectation.fulfill()
            }

        viewModel.dataViewModels.signal
            .observeValues {
                guard case .movie(let movie)? = $0.first else { return XCTFail("Unexpected missing section data.") }
                movie.tapped.apply().start()
            }

        viewModel.searchTerm.value = "venom"

        waitForExpectations(timeout: 1.0)
    }
}


