import Foundation
import XCTest
import ReactiveSwift
@testable import Moviefy


class DetailsViewModelTestCase: XCTestCase {
    
    private var viewModel: DetailsViewModel!
    private var store: MockStore!
    
    
    // MARK: - Setup
    
    override func setUp() {
        super.setUp()
        let firstMovie = TMDBMovie(title: "movie1", id: 1)

        store = MockStore()
        viewModel = DetailsViewModel(movie: firstMovie, store: store)
    }
    
    override func tearDown() {
        
        viewModel = nil
        
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testDetails_WhenMovieHave3Fields_ShouldHave3Cells() {
        let firstMovie = TMDBMovie(title: "movie2",
                                   id: 1,
                                   voteAverage: 3.0,
                                   overview: "Lorem ipsum")
        
        let dataExpectation = expectation(description: "dataExpectation")
        
        store.mockGetMovie = .success(firstMovie)
        
        viewModel.dataViewModels.signal
            .observeValues {
                XCTAssertEqual($0.count, 3)
                dataExpectation.fulfill()
        }
        
        viewModel.load()
        
        waitForExpectations(timeout: 1.0)
    }
    
    func testDetails_WhenMovieHave5Fields_ShouldHave5Cells() {
        let firstMovie = TMDBMovie(title: "movie2",
                                   id: 1,
                                   budget: 3000,
                                   voteAverage: 3.0,
                                   status: "Live",
                                   overview: "Lorem ipsum")
        
        let dataExpectation = expectation(description: "dataExpectation")
        
        store.mockGetMovie = .success(firstMovie)
        
        viewModel.dataViewModels.signal
            .observeValues {
                XCTAssertEqual($0.count, 5)
                dataExpectation.fulfill()
        }
        
        viewModel.load()
        
        waitForExpectations(timeout: 1.0)
    }
}


