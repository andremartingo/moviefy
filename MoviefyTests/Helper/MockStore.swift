import Foundation
@testable import Moviefy

class MockStore: StoreRepresentable {
    
    var mockGetMovie: Result<TMDBMovie, MovieError> = .failure(MovieError.network)
    var mockGetPopularMovies: Result<[TMDBMovie], MovieError> = .failure(MovieError.network)
    var mockSearchMovies: Result<[TMDBMovie], MovieError> = .failure(MovieError.network)
    
    func getMovie(movieID: Int, _ completionHandler: @escaping (Result<TMDBMovie, MovieError>) -> Void) {
        completionHandler(mockGetMovie)
    }
    
    func getPopularMovies(_ completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void) {
        completionHandler(mockGetPopularMovies)

    }
    
    func getImage(imageSize: API.ImageSize, imagePath: String, _ completionHandler: @escaping (Result<Data, MovieError>) -> Void) {
        return
    }
    
    func searchMovies(with term: String, completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void) {
        completionHandler(mockSearchMovies)
    }
    
    
}
