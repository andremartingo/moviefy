import Foundation

enum Result<Value, Error: Swift.Error> {
    case success(Value)
    case failure(Error)
}

enum MovieError: Error {
    case network
    case parsing
}
