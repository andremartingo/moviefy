import Foundation

protocol NetworkProviderRepresentable {
    func request(resource: Resource, completion: @escaping (Result<Data, MovieError>) -> Void)
}

final class NetworkProvider: NetworkProviderRepresentable {
    
    // MARK: - Properties
    
    private let session: URLSession
    
    // MARK: - Lifecycle
    
    init(session: URLSession = URLSession(configuration: .default)) {
        
        self.session = session
    }
    
    func request(resource: Resource,
                        completion: @escaping (Result<Data, MovieError>) -> Void) {
        session.dataTask(with: resource.request()) { (data, urlresponse, error) in
            if error != nil {
                completion(.failure(MovieError.network))
            }
            if let data = data {
                completion(.success(data))
            }
        }.resume()
    }
}
