import Foundation

protocol StoreRepresentable {
    func getMovie(movieID: Int, _ completionHandler: @escaping (Result<TMDBMovie, MovieError>) -> Void)
    func getPopularMovies(_ completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void)
    func getImage(imageSize: API.ImageSize, imagePath: String,_ completionHandler: @escaping (Result<Data, MovieError>) -> Void)
    func searchMovies(with term: String, completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void)
}

final class Store: StoreRepresentable {
    
    let networkProvider: NetworkProviderRepresentable
    
    init(networkProvider: NetworkProviderRepresentable) {
        self.networkProvider = networkProvider
    }
    
    func getMovie(movieID: Int, _ completionHandler: @escaping (Result<TMDBMovie, MovieError>) -> Void) {
        
        let endpoint = API.Endpoint.details(id: movieID)
        let popularResource = Resource(baseURL: API.baseURL,
                                       path: endpoint.path,
                                       method: endpoint.method,
                                       query: endpoint.query)
        
        networkProvider.request(resource: popularResource) {
            switch $0 {
            case .success(let data):
                do {
                    let movie = try JSONDecoder().decode(TMDBMovie.self, from: data)
                    completionHandler(.success(movie))
                } catch {
                    completionHandler(.failure(MovieError.parsing))
                }
            case .failure:
                completionHandler(.failure(MovieError.network))
            }
        }
    }
    
    func getPopularMovies(_ completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void) {
        
        let endpoint = API.Endpoint.popular
        let popularResource = Resource(baseURL: API.baseURL,
                                       path: endpoint.path,
                                       method: endpoint.method,
                                       query: endpoint.query)
        
        networkProvider.request(resource: popularResource) {
            switch $0 {
            case .success(let data):
                do {
                    let movie = try JSONDecoder().decode(MovieResponse.self, from: data)
                    completionHandler(.success(movie.results))
                } catch {
                    completionHandler(.failure(MovieError.parsing))
                }
            case .failure:
                completionHandler(.failure(MovieError.network))
            }
        }
    }
    
    func searchMovies(with term: String, completionHandler: @escaping (Result<[TMDBMovie], MovieError>) -> Void) {
                
        let endpoint = API.Endpoint.search(term: term)
        let popularResource = Resource(baseURL: API.baseURL,
                                       path: endpoint.path,
                                       method: endpoint.method,
                                       query: endpoint.query)
        
        networkProvider.request(resource: popularResource) {
            switch $0 {
            case .success(let data):
                do {
                    let movie = try JSONDecoder().decode(MovieResponse.self, from: data)
                    completionHandler(.success(movie.results))
                } catch {
                    completionHandler(.failure(MovieError.parsing))
                }
            case .failure:
                completionHandler(.failure(MovieError.network))
            }
        }
    }
    
    func getImage(imageSize: API.ImageSize ,imagePath: String,_ completionHandler: @escaping (Result<Data, MovieError>) -> Void) {
        
        let endpoint = API.Endpoint.image(id: imagePath, size: imageSize)
        let imageResource = Resource(baseURL: API.imageURL,
                                     path: endpoint.path,
                                     method: endpoint.method,
                                     query: endpoint.query)
        
        networkProvider.request(resource: imageResource) {
            switch $0 {
            case .success(let data):
                completionHandler(Result.success(data))
            case .failure:
                completionHandler(Result.failure(MovieError.network))
            }
        }
    }
}
