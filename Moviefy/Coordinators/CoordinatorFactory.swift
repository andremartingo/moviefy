import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result

struct CoordinatorFactory {
    
    static func appCoordinator(window: UIWindow) -> WindowCoordinator {
        
        let starter: () -> ChildCoordinator = {
            let tabBarController = UITabBarController()
            
            let coordinator = makeTabBarCoordinator(tabBarController: tabBarController)
            
            let child = ChildCoordinator(viewController: tabBarController,
                                         coordinator: coordinator,
                                         identifier: Identifiers.navigation)
            
            return child
        }
        
        return WindowCoordinator(window: window, starter: starter)
    }
    
    static func makeTabBarCoordinator(tabBarController: UITabBarController) -> TabBarControllerCoordinator {
        
        let starter: () -> [ChildCoordinator] = {
            
            let networkProvider = NetworkProvider()
            let store = Store(networkProvider: networkProvider)
            
            let homeNavigationController = UINavigationController()
            let homeCoordinator = makeHomeCoordinator(navigationController: homeNavigationController, store: store)
            
            let searchNavigationController = UINavigationController()
            let searchCoordinator = makeSearchCoordinator(navigationController: searchNavigationController, store: store)
            
            let tabs = [ChildCoordinator(viewController: homeNavigationController,
                                         coordinator: homeCoordinator,
                                         identifier: Identifiers.home),
                        ChildCoordinator(viewController: searchNavigationController,
                                         coordinator: searchCoordinator,
                                         identifier: Identifiers.search)]
            return tabs
        }
        
        return TabBarControllerCoordinator(tabBarController: tabBarController, prepareStart: starter)
        
    }

    
    static func makeHomeCoordinator(navigationController: UINavigationController, store: StoreRepresentable)
        -> HomeCoordinator<HomeViewModel> {
            
            let starter: () -> HomeViewController<HomeViewModel> = {
                let viewModel = HomeViewModel(store: store)
                let viewController = HomeViewController(viewModel: viewModel)
                
                viewController.tabBarItem = UITabBarItem(tabBarSystemItem: .featured, tag: 0)
                return viewController
            }
            
            return HomeCoordinator(navigationController: navigationController, starter: starter)
    }
    
    static func makeSearchCoordinator(navigationController: UINavigationController, store: StoreRepresentable)
        -> SearchCoordinator<SearchViewModel> {
            
            let starter: () -> SearchViewController<SearchViewModel> = {
                let viewModel = SearchViewModel(store: store)
                let viewController = SearchViewController(viewModel: viewModel)
                viewController.definesPresentationContext = true
                
                viewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
                return viewController
            }
            
            return SearchCoordinator(navigationController: navigationController, starter: starter)
    }
    
    static func makeDetailsCoordinator(navigationController: UINavigationController,
                                       store: StoreRepresentable,
                                       movie: TMDBMovie)
        -> DetailsCoordinator<DetailsViewModel> {
            
            let starter: () -> DetailsViewController<DetailsViewModel> = {
                let viewModel = DetailsViewModel(movie: movie, store: store)
                let viewController = DetailsViewController(viewModel: viewModel)
                
                return viewController
            }
            
            return DetailsCoordinator(navigationController: navigationController, starter: starter)
    }
    
    
    // MARK: - Identifiers
    
    private struct Identifiers {
        
        static let home = "home"
        static let search = "search"
        static let navigation = "navigation"
    }
}
