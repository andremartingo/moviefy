import UIKit

class NavigationControllerCoordinator<ViewModel, ViewController>: Coordinator
where ViewController: Moviefy.ViewController<ViewModel> {
    
    // MARK: - Properties
    
    private(set) var childCoordinators: [Coordinator.Identifier: Coordinator] = [:]
    
    let navigationController: UINavigationController
    
    private(set) var viewController: ViewController?
    {
        willSet {
            if let newViewController = newValue {
                newViewController.onDismiss = { [weak self] in self?.finish(animated: false) }
            } else {
                viewController?.onDismiss = nil
            }
        }
    }
    
    private let starter: () -> ViewController
    
    // MARK: - Lifecycle
    
    init(navigationController: UINavigationController, starter: @escaping () -> ViewController) {
        
        self.navigationController = navigationController
        self.starter = starter
    }
    
    func start(animated: Bool) {
        
        viewController = starter()
        
        setUpNavigation()
        
        pushViewController(animated: animated)
    }
    
    func finish(animated: Bool) {

        let children = self.childCoordinators
        self.childCoordinators = [:]
        children.forEach { $0.value.finish(animated: false) }
        
        viewController?.onDismiss = nil
        
        cleanUpNavigation()
        
        viewController = nil
    }
    
    // MARK: - Helpers
    
    func setUpNavigation() {}
    
    func cleanUpNavigation() {}
    
    func pushViewController(animated: Bool) {
        
        guard let viewController = viewController else {
            return
        }
        
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    func start(child: Coordinator,
               identifier: Identifier,
               animated: Bool) {
        
        childCoordinators[identifier] = child
        child.start(animated: animated)
    }
}
