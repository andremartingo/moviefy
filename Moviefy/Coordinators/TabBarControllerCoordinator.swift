import UIKit
import enum Result.NoError

final class TabBarControllerCoordinator: NSObject, Coordinator {
    
    
    private(set) var childCoordinators: [Identifier : Coordinator] = [:]
        
    private var tabChildCoordinators: [ChildCoordinator] = []
    
    let tabBarController: UITabBarController
    
    private let prepareStart: () -> [ChildCoordinator]
    
    init(tabBarController: UITabBarController, prepareStart: @escaping () -> [ChildCoordinator]) {
        self.tabBarController = tabBarController
        self.prepareStart = prepareStart
        
        super.init()
    }
    
    func start(animated: Bool) {

        let tabChilds = prepareStart()
        tabChildCoordinators = tabChilds
        
        setViewControllers(vcs: tabChilds.map { $0.viewController }, animated: animated)
        
        tabChilds.forEach { startChildCoordinator($0.coordinator, identifier: $0.identifier, animated: animated) }
    }
    
    func finish(animated: Bool) {

        let childCoordinators = self.childCoordinators
        self.childCoordinators = [:]
        childCoordinators.forEach { $0.value.finish(animated: false) }

        clearViewControllers(animated: animated)
        
        tabChildCoordinators = []

    }
    
    // MARK: Helpers
    
    func setViewControllers(vcs: [UIViewController], animated: Bool) {
        
        tabBarController.setViewControllers(vcs, animated: animated)
    }
    
    func clearViewControllers(animated: Bool) {

        tabBarController.setViewControllers([], animated: animated)
    }
    
    // MARK: - Child lifecycle
    
    func startChildCoordinator(_ childCoordinator: Coordinator,
                               identifier: Identifier,
                               animated: Bool,
                               onChildFinish: (() -> Void)? = nil) {

        childCoordinators[identifier] = childCoordinator
        childCoordinator.start(animated: animated)
    }
}
