import UIKit
import ReactiveSwift

final class DetailsCoordinator<ViewModel: DetailsViewModelRepresentable>:
NavigationControllerCoordinator<ViewModel,DetailsViewController<ViewModel>>{
    
    // MARK: - Properties
    
    private let (lifetime, token) = Lifetime.make()
    private var disposable: CompositeDisposable?
    
    // MARK: - Lifecycle
    
    override init(navigationController: UINavigationController, starter: @escaping () -> DetailsViewController<ViewModel>) {
        
        super.init(navigationController: navigationController, starter: starter)
    }
    
    override func cleanUpNavigation() {
        super.cleanUpNavigation()
        
        disposable?.dispose()
        disposable = nil
    }
}


// MARK: - Identifiers

private struct Identifiers {
    
    static let details = "details"
}

