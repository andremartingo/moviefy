import UIKit
import ReactiveSwift

final class HomeCoordinator<ViewModel: HomeViewModelRepresentable>:
NavigationControllerCoordinator<ViewModel,HomeViewController<ViewModel>>{
    
    // MARK: - Properties
    
    private let (lifetime, token) = Lifetime.make()
    private var disposable: CompositeDisposable?
    
    // MARK: - Lifecycle
    
    override init(navigationController: UINavigationController, starter: @escaping () -> HomeViewController<ViewModel>) {
        
        super.init(navigationController: navigationController, starter: starter)
    }

    override func setUpNavigation() {
        super.setUpNavigation()
        
        guard let viewController = viewController else { return }
        
        let disposable = CompositeDisposable()
        defer { self.disposable = disposable }
        
        disposable += viewController.viewModel.navigation
            .take(during: lifetime)
            .observe(on: QueueScheduler.main)
            .observeValues { [weak self] in
                switch $0 {
                case .showMovieDetails(let movie, let store):
                    self?.startDetailsCoordinator(movie: movie, store: store)
                }
            }
    }
    
    override func cleanUpNavigation() {
        super.cleanUpNavigation()
        
        disposable?.dispose()
        disposable = nil
    }
    
    // MARK: - Details Coordinator
    
    private func startDetailsCoordinator(movie: TMDBMovie, store: StoreRepresentable) {
        
        let coordinator = CoordinatorFactory.makeDetailsCoordinator(navigationController: navigationController,
                                                                    store: store,
                                                                    movie: movie)
        
        start(child: coordinator, identifier: Identifiers.details, animated: true)
    }
}


// MARK: - Identifiers

private struct Identifiers {
    
    static let details = "details"
}

