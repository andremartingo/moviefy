import UIKit
import ReactiveCocoa

final class WindowCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private(set) var childCoordinators: [Coordinator.Identifier: Coordinator] = [:]
    
    let window: UIWindow
    
    private let starter: () -> ChildCoordinator
    
    // MARK: - Lifecycle
    
    init(window: UIWindow, starter: @escaping () -> ChildCoordinator) {
        
        self.window = window
        self.starter = starter
    }
    
    func start(animated: Bool) {
        
        let root = starter()
        
        setUpNavigation()
        
        window.rootViewController = root.viewController
        window.makeKeyAndVisible()
                
        start(child: root.coordinator, identifier: root.identifier, animated: animated)
    }
    
    func finish(animated: Bool) {}
    
    func setUpNavigation() {}
    
    func cleanUpNavigation() {}
    
    func start(child: Coordinator,
               identifier: Identifier,
               animated: Bool) {
        
        childCoordinators[identifier] = child
        child.start(animated: animated)
    }
}
