import UIKit

struct ChildCoordinator {
    
    let viewController: UIViewController
    let coordinator: Coordinator
    let identifier: Coordinator.Identifier
}
