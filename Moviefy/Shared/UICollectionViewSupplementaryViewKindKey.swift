import UIKit

struct UICollectionViewSupplementaryViewKindKey: RawRepresentable, Hashable {
    
    // MARK: - Properties
    
    let rawValue: String
    
    // MARK: - Lifecycle
    
    init(rawValue: String) {
        
        self.rawValue = rawValue
    }
    
    init(_ rawValue: String) {
        
        self.init(rawValue: rawValue)
    }
}

extension UICollectionViewSupplementaryViewKindKey: ExpressibleByStringLiteral {
    
    init(stringLiteral value: String) {
        self.init(value)
    }
}

extension UICollectionViewSupplementaryViewKindKey {
    static let header = UICollectionViewSupplementaryViewKindKey(UICollectionView.elementKindSectionHeader)
    static let footer = UICollectionViewSupplementaryViewKindKey(UICollectionView.elementKindSectionFooter)
}
