import Foundation
import UIKit
import ReactiveCocoa
import ReactiveSwift

class CollectionViewCell<ViewModel>: UICollectionViewCell {
    var viewModel: ViewModel? {
        didSet {
            setUpBindings()
        }
    }
    func setUpSubviews(){}
    func setUpConstraints(){}
    func setUpBindings(){}
}

class TableViewCell<ViewModel>: UITableViewCell {
    var viewModel: ViewModel? {
        didSet {
            setUpBindings()
        }
    }
    func setUpSubviews(){}
    func setUpConstraints(){}
    func setUpBindings(){}
}

