import ReactiveSwift
import enum Result.NoError

extension Signal.Observer {
    
    @discardableResult
    static func <~ <Source: BindingSource>(observer: Signal<Value, Error>.Observer, source: Source) -> Disposable?
        where Source.Value == Value{
            
            return source.producer.start { [weak observer] in
                switch $0 {
                case .value(let value):
                    observer?.send(value: value)
                case .failed, .interrupted, .completed:
                    break
                }
            }
    }
}

