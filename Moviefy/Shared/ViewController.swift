import UIKit
import ReactiveSwift
import ReactiveCocoa

class ViewController<ViewModel: Moviefy.ViewModel>: UIViewController {
    
    // MARK: - Properties
    
    let viewModel: ViewModel
    
    var onDismiss: (() -> Void)?
    
    // MARK: - Lifecycle
    
    init(viewModel: ViewModel) {
        
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("Use init(viewModel:) instead.")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setUpSubviews()
        setUpConstraints()
        setUpBindings()
        
        viewModel.load()
    }
    
    override func didMove(toParent parent: UIViewController?) {
        
        super.didMove(toParent: parent)
        
        guard parent == nil else { return }
        onDismiss?()
    }
    
    func setUpSubviews() {
    }

    func setUpConstraints() {
    
    }
    
    func setUpBindings() {
    }
    
}
