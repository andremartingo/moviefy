import UIKit

protocol Coordinator: class {
    
    typealias Identifier = String
    
    var childCoordinators: [Identifier: Coordinator] { get }
    
    func start(animated: Bool)
    func finish(animated: Bool)
}

struct ChildCoordinator {
    
    let viewController: UIViewController
    let coordinator: Coordinator
    let identifier: Coordinator.Identifier
}
