precedencegroup ForwardApplication {
    associativity: left
    higherThan: AssignmentPrecedence
}

infix operator |> : ForwardApplication

// MARK: - Forward function application

/// Forward function application.
///
/// Applies the function on the right to the value on the left.
/// Functions of >1 argument can be applied by placing their arguments in a tuple on the left hand side.
///
/// This is a useful way of clarifying the flow of data through a series of functions.
/// For example, you can use this to count the base-10 digits of an integer:
///
///     `let digits = 100 |> toString |> count // => 3`
func |> <A, B> (a: A, f: (A) -> B) -> B {
    
    return f(a)
}

@discardableResult
func |> <A: AnyObject> (a: A, f: (A) -> Void) -> A {
    
    f(a)
    return a
}

infix operator ?|> : ForwardApplication

func ?|> <A, B> (a: A?, f: (A) -> B) -> B? {
    
    guard let a = a else { return nil }
    
    return f(a)
}

@discardableResult
func ?|> <A: AnyObject> (a: A?, f: (A) -> Void) -> A? {
    
    guard let a = a else { return nil }
    
    f(a)
    return a
}
