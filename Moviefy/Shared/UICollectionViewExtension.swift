import UIKit

extension UICollectionView {
    
    // MARK: - Cells
    
    final func register<T: UICollectionViewCell>(cell: T.Type) {
        
        register(cell.self, forCellWithReuseIdentifier: cell.reuseIdentifier)
    }
    
    final func dequeueCell<T: UICollectionViewCell>(for indexPath: IndexPath, cell: T.Type = T.self) -> T {
        
        let anyCell = dequeueReusableCell(withReuseIdentifier: cell.reuseIdentifier, for: indexPath)
        
        guard let typedCell = anyCell as? T else {
            fatalError(
                "Failed to dequeue cell with identifier \(cell.reuseIdentifier) matching type \(cell.self)."
            )
        }
        
        return typedCell
    }
    
    // MARK: - Supplementary Views
    
    final func register<T: UICollectionReusableView>(supplementaryView: T.Type, ofKind elementKind: UICollectionViewSupplementaryViewKindKey) {
        
        register(supplementaryView.self,
                 forSupplementaryViewOfKind: elementKind.rawValue,
                 withReuseIdentifier: supplementaryView.reuseIdentifier)
    }
    
    final func dequeueSupplementaryView<T: UICollectionReusableView>(ofKind elementKind: UICollectionViewSupplementaryViewKindKey,
                                                                     for indexPath: IndexPath,
                                                                     viewType: T.Type = T.self) -> T {
        
        let anyView = dequeueReusableSupplementaryView(ofKind: elementKind.rawValue,
                                                       withReuseIdentifier: viewType.reuseIdentifier,
                                                       for: indexPath)
        guard let typedView = anyView as? T else {
            fatalError(
                "Failed to dequeue supplementary view with identifier \(viewType.reuseIdentifier) matching type \(viewType.self)."
            )
        }
        
        return typedView
    }
}
