import ReactiveSwift

protocol ViewModel {
    
    var title: Property<String?> { get set }
    
    func load()
}

extension ViewModel {
    
    var title: Property<String?> { return Property(value: nil) }
    
    func load() {}
}
