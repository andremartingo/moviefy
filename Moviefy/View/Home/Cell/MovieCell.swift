import Foundation
import ReactiveSwift
import UIKit

class MovieCell<ViewModel>: CollectionViewCell<ViewModel> where ViewModel: MovieViewModel {
    
    private let imageView: UIImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpSubviews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setUpSubviews() {
        addSubview(imageView)
        imageView.edges(to: contentView)
        imageView.image = UIImage(named: "Film")
        imageView.contentMode = .scaleToFill
    }
    
    override func setUpBindings(){
        guard let viewModel = self.viewModel else {
            fatalError("💥 : Missing ViewModel")
        }

        imageView.reactive.image <~ viewModel.image.producer
            .take(until: reactive.prepareForReuse)
            .observe(on: QueueScheduler.main)
            .filter{ $0 != nil }
            .map { data -> UIImage in
                guard
                    let data = data,
                    let image = UIImage(data: data)
                else { return UIImage() }
                return image
            }
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        imageView.image = nil
    }
}
