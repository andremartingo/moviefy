import Foundation
import ReactiveSwift
import UIKit

final class InfoCell<ViewModel>: TableViewCell<ViewModel> where ViewModel: InfoViewModel {
    
    let titleLabel: UILabel = UILabel()
    let infoLabel: UILabel = UILabel()
        |> Styles.infoLabel

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpSubviews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setUpSubviews(){
        addSubview(titleLabel)
        addSubview(infoLabel)
    }

    override func setUpConstraints() {
        titleLabel.centerYToSuperview()
        titleLabel.leadingToSuperview(offset: 15)
        titleLabel.width(Constants.TitleLabel.width)
        infoLabel.centerYToSuperview()
        infoLabel.leadingToTrailing(of: titleLabel)
        infoLabel.trailingToSuperview(offset: 15)
    }
    
    override func setUpBindings(){
        guard let viewModel = self.viewModel else {
            fatalError("💥 : Missing ViewModel")
        }
        titleLabel.reactive.text <~ viewModel.title.producer
            .take(until: reactive.prepareForReuse)
        
        infoLabel.reactive.text <~ viewModel.info.producer
            .take(until: reactive.prepareForReuse)
    }
}

private enum Styles {
    
    static let infoLabel : (UILabel) -> Void = {
        $0.textAlignment = .right
    }
}

private enum Constants {
    
    enum TitleLabel {
        static let width: CGFloat = 100
    }
}
