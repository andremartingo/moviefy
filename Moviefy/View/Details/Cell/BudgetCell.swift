import Foundation
import ReactiveSwift
import UIKit

class BudgetCell<ViewModel>: TableViewCell<ViewModel> where ViewModel: InfoViewModel {
    
    let titleLabel: UILabel = UILabel()
    let infoLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpSubviews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setUpSubviews(){
        addSubview(titleLabel)
        addSubview(infoLabel)
    }
    
    override func setUpConstraints() {
        titleLabel.centerYToSuperview()
        titleLabel.leadingToSuperview(offset: 15)
        infoLabel.centerYToSuperview()
        infoLabel.trailingToSuperview(offset: 15)
    }
    
    override func setUpBindings(){
        guard let viewModel = self.viewModel else {
            fatalError("💥 : Missing ViewModel")
        }
        titleLabel.reactive.text <~ viewModel.title.producer
            .take(until: reactive.prepareForReuse)
        
        infoLabel.reactive.text <~ viewModel.info.producer
            .filterMap { [weak self] in
                guard let budget = $0 else { return nil}
                return self?.makeBudgetString(string: budget)
            }
    }
    
    private func  makeBudgetString(string: String) -> String {
        guard let price = Double(string) else { return "" }
        
        let formatter = NumberFormatter() |> {
            $0.numberStyle = .currency
            $0.locale = Locale(identifier: "de_DE")
            $0.currencyCode = "EUR"
            $0.currencySymbol = "€"
            $0.usesGroupingSeparator = true
            $0.maximumFractionDigits = 0
        }
    
        return formatter.string(from: price as NSNumber) ?? ""
    }
}
