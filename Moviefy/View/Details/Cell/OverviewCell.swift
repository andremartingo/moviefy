import UIKit
import ReactiveSwift
import Foundation

class OverviewCell<ViewModel>: TableViewCell<ViewModel> where ViewModel: OverviewViewModel {
    
    let infoLabel: UILabel = UILabel()
        |> Styles.label
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpSubviews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setUpSubviews(){
        addSubview(infoLabel)
    }
    
    override func setUpConstraints() {
        infoLabel.edgesToSuperview(insets: UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15))
    }
    
    override func setUpBindings(){
        guard let viewModel = self.viewModel else {
            fatalError("💥 : Missing ViewModel")
        }
        infoLabel.reactive.text <~ viewModel.overview.producer
            .take(until: reactive.prepareForReuse)
    }
}

private enum Styles {
    
    static let label: (UILabel) -> Void = {
        $0.numberOfLines = 0
    }
}
