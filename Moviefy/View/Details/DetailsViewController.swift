import UIKit
import ReactiveSwift
import ReactiveCocoa
import TinyConstraints

final class DetailsViewController<ViewModel>: ViewController<ViewModel>, UITableViewDataSource, UINavigationControllerDelegate, UITableViewDelegate
where ViewModel: DetailsViewModelRepresentable{
    
    // MARK: - Properties
    
    private let imageView: UIImageView = UIImageView()
        |> Styles.imageView
    
    let tableView: UITableView = UITableView()
        |> Styles.tableView
    
    private let (lifetime,token) = Lifetime.make()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: InfoCell.self)
        tableView.register(cellType: OverviewCell.self)
        tableView.register(cellType: BudgetCell.self)
        
        tableView.reactive.reloadData <~ viewModel.dataViewModels.signal.map { _ in }

        navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
    }
    
    override func setUpSubviews() {
        view.addSubview(imageView)
        view.addSubview(tableView)
    }
    
    override func setUpConstraints() {
        imageView.topToSuperview()
        imageView.leadingToSuperview()
        imageView.trailingToSuperview()
        imageView.height(400 + navigationController!.navigationBar.frame.height)
        tableView.topToSuperview(usingSafeArea: true)
        tableView.leadingToSuperview()
        tableView.trailingToSuperview()
        tableView.bottomToSuperview()
    }
    
    override func setUpBindings() {
        navigationItem.reactive.title <~ viewModel.title
        
        imageView.reactive.image <~ viewModel.image.producer
            .take(during: lifetime)
            .observe(on: QueueScheduler.main)
            .filter{ $0 != nil }
            .map { data -> UIImage in
                guard
                    let data = data,
                    let image = UIImage(data: data)
                    else { return UIImage() }
                return image
            }
    }
    
    // MARK: - UITableViewDataSource
    
//    func tableView(_: UITableView,
//                   titleForHeaderInSection _: Int) -> String? {
//        return " "
//    }
//
//    func numberOfSections(in _: UITableView) -> Int {
//        return viewModel.dataViewModels.value.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = self.getViewModel(at: indexPath)
        switch viewModel {
        case .info(let viewModel):
            let cell: InfoCell<InfoViewModel> = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = viewModel
            return cell
        case .overview(let viewModel):
            let cell: OverviewCell<OverviewViewModel> = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = viewModel
            return cell
        case .budget(let viewModel):
            let cell: BudgetCell<InfoViewModel> = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = viewModel
            return cell
        }
    }
    
    // MARK: - Helpers
    
    private func getViewModel(at indexpath: IndexPath) -> DetailsCellViewModel {
        return viewModel.dataViewModels.value[indexpath.row]
    }
    
}

private enum Styles {
    
    static let tableView: (UITableView) -> Void = {
        $0.frame = .zero
        $0.clipsToBounds = false
        $0.backgroundColor = .clear
        $0.contentInset = UIEdgeInsets(top: 400, left: 0, bottom: 0, right: 0)
        $0.contentOffset = CGPoint(x: 0, y: -400)
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.tableFooterView = UIView()
        $0.estimatedRowHeight = 40
        $0.rowHeight = UITableView.automaticDimension
    }
    
    static let imageView: (UIImageView) -> Void = {
        $0.contentMode = .scaleToFill
    }
}


private enum Constants {
    
    enum Expanded {
        static let size = UIApplication.shared.statusBarFrame.height
    }
    
    enum Cell {
        enum Margins {
            static let verticalSpacing = CGFloat(2)
            static let horizontalSpacing = CGFloat(2)
        }
        enum Size {
            static let singleColumnSpanSize = CGSize(width: 0.5 *
                (UIScreen.main.bounds.width - Constants.Cell.Margins.horizontalSpacing), height: 280)
        }
    }
}
