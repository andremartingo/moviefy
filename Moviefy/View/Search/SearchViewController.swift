import UIKit
import ReactiveSwift
import ReactiveCocoa
import TinyConstraints

final class SearchViewController<ViewModel>: ViewController<ViewModel>, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
where ViewModel: SearchViewModelRepresentable{
    
    // MARK: - Properties
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = Constants.Cell.Margins.horizontalSpacing
        layout.minimumInteritemSpacing = Constants.Cell.Margins.verticalSpacing
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    private let (lifetime,token) = Lifetime.make()
    
    // MARK: - Type Aliases
    
    private typealias MovieCollectionViewCell = MovieCell<MovieViewModel>
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cell: MovieCollectionViewCell.self)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
        
        collectionView.reactive.reloadData <~ viewModel.dataViewModels.signal.map { _ in }
        
        viewModel.searchTerm <~ searchController.searchBar.reactive.continuousTextValues
            .throttle(0.5, on:  QueueScheduler.main)
            .skipNil()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationController()
    }
    
    func setupNavigationController() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.view.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func setUpSubviews() {
        view.addSubview(collectionView)
    }
    
    override func setUpConstraints() {
        collectionView.edgesToSuperview()
    }
    
    override func setUpBindings() {
        navigationItem.reactive.title <~ viewModel.title
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataViewModels.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let viewModel = self.getViewModel(at: indexPath)
        
        switch viewModel {
        case .movie(let viewModel):
            let cell: MovieCell = collectionView.dequeueCell(for: indexPath)
            cell.viewModel = viewModel
            return cell
        }
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewModel = self.getViewModel(at: indexPath)
        switch viewModel {
        case .movie(let viewModel):
            viewModel.tapped.apply().start()
        }
    }
    
    // MARK: - UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Constants.Cell.Size.singleColumnSpanSize
    }
    
    // MARK: - Helpers
    
    private func getViewModel(at indexpath: IndexPath) -> SearchCellViewModel {
        return viewModel.dataViewModels.value[indexpath.row]
    }
    
}

private enum Constants {
    
    enum Cell {
        enum Margins {
            static let verticalSpacing = CGFloat(2)
            static let horizontalSpacing = CGFloat(2)
        }
        enum Size {
            static let singleColumnSpanSize = CGSize(width: 0.5 *
                (UIScreen.main.bounds.width - Constants.Cell.Margins.horizontalSpacing), height: 280)
        }
    }
}
