import Foundation
import ReactiveSwift
import Result

class MovieViewModel {
    
    let title: Property<String>
    let tapped: Action<Void,Void,NoError>
    let image: MutableProperty<Data?>
    private let (lifetime, token) = Lifetime.make()
    
    init(for movie: TMDBMovie, store: StoreRepresentable) {
        image = .init(nil)
        title = .init(value: "Teste")
        tapped = Action { SignalProducer(value: ()) }
        if let path = movie.posterPath {
            store.getImage(imageSize: .small, imagePath: path) { [weak self] in
                switch $0 {
                case .success(let data):
                    self?.image.value = data
                case .failure(let error):
                    print("🚨 : \(error)")
                }
            }
        }
    }
}
