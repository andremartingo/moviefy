import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result
import enum Result.NoError


enum HomeCellViewModel {
    
    case movie(MovieViewModel)
}

enum HomeNavigation {
    
    case showMovieDetails(TMDBMovie,StoreRepresentable)
}

protocol HomeViewModelRepresentable: ViewModel {
    
    var dataViewModels : MutableProperty<[HomeCellViewModel]> {get}
    var navigation: Signal<HomeNavigation, NoError> {get}
    
}


class HomeViewModel: HomeViewModelRepresentable {
    
    // MARK: - Properties
    
    var title: Property<String?>
    var dataViewModels : MutableProperty<[HomeCellViewModel]>
    private let store: StoreRepresentable
    let navigation: Signal<HomeNavigation, NoError>
    private let navigationObserver: Signal<HomeNavigation, NoError>.Observer
    
    private let (lifetime,token) = Lifetime.make()
    
    // MARK: - Lifecycle
    
    init(store: StoreRepresentable) {
        title = .init(value: "Popular Movies")
        dataViewModels = .init([])
        self.store = store
        (navigation, navigationObserver) = Signal.pipe()
    }
    
    public func load(){
        store.getPopularMovies { [weak self] in
            switch $0 {
            case .success(let movies):
                guard let strongSelf = self else { return }
                strongSelf.dataViewModels.value = strongSelf.makeDataViewModels(for: movies)
            case .failure(let error):
                print("🚨 : \(error)")
            }
        }
    }
    
    // MARK: - Helpers
    
    private func makeDataViewModels(for movies: [TMDBMovie]) -> [HomeCellViewModel] {
        return movies.map { movie in
            let cellViewModel = MovieViewModel(for: movie, store: store)
            navigationObserver <~ cellViewModel.tapped.values
                .take(during: lifetime)
                .map { _ in
                    return .showMovieDetails(movie, self.store)
                }
            return .movie(cellViewModel)
        }
    }
}
