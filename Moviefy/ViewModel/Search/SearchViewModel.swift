import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result
import enum Result.NoError


enum SearchCellViewModel {
    
    case movie(MovieViewModel)
}

enum SearchNavigation {
    
    case showMovieDetails(TMDBMovie,StoreRepresentable)
}

protocol SearchViewModelRepresentable: ViewModel {
    
    var dataViewModels : MutableProperty<[SearchCellViewModel]> {get}
    var navigation: Signal<SearchNavigation, NoError> {get}
    var searchTerm: MutableProperty<String> { get }
    
}


class SearchViewModel: SearchViewModelRepresentable {
    
    // MARK: - Properties
    
    var title: Property<String?>
    var searchTerm: MutableProperty<String>
    var dataViewModels : MutableProperty<[SearchCellViewModel]>
    private let store: StoreRepresentable
    let navigation: Signal<SearchNavigation, NoError>
    private let navigationObserver: Signal<SearchNavigation, NoError>.Observer
    
    private let (lifetime,token) = Lifetime.make()
    
    // MARK: - Lifecycle
    
    init(store: StoreRepresentable) {
        title = .init(value: "Search")
        dataViewModels = .init([])
        searchTerm = .init("")
        self.store = store
        (navigation, navigationObserver) = Signal.pipe()
        setUpBindingsAndObservers()
    }
    
    private func setUpBindingsAndObservers(){
        searchTerm.signal
            .filter{ $0.count > 0 }
            .observeValues { [weak self] in
            self?.store.searchMovies(with: $0) { [weak self] result in
                switch result {
                case .success(let movies):
                    guard let strongSelf = self else { return }
                    strongSelf.dataViewModels.value = strongSelf.makeDataViewModels(for: movies)
                case .failure(let error):
                    print("🚨 : \(error)")
                }
            }
        }
    }
    
    // MARK: - Helpers
    
    private func makeDataViewModels(for movies: [TMDBMovie]) -> [SearchCellViewModel] {
        return movies.map { movie in
            let cellViewModel = MovieViewModel(for: movie, store: store)
            navigationObserver <~ cellViewModel.tapped.values
                .take(during: lifetime)
                .map { _ in
                    return .showMovieDetails(movie, self.store)
                }
            return .movie(cellViewModel)
        }
    }
}
