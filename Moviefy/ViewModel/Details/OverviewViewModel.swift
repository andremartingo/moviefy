import Foundation
import ReactiveSwift

class OverviewViewModel {
    
    let overview: Property<String?>
    
    init(overview: String) {
        self.overview = .init(value: overview)
    }
}
