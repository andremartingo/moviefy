import Foundation
import ReactiveSwift
import ReactiveCocoa
import Result
import enum Result.NoError


enum DetailsCellViewModel {
    
    case overview(OverviewViewModel)
    case info(InfoViewModel)
    case budget(InfoViewModel)
}

protocol DetailsViewModelRepresentable: ViewModel {
    
    var dataViewModels : MutableProperty<[DetailsCellViewModel]> {get}
    var image: MutableProperty<Data?> { get }
}


class DetailsViewModel: DetailsViewModelRepresentable {
    
    // MARK: - Properties
    
    var title: Property<String?>
    var dataViewModels : MutableProperty<[DetailsCellViewModel]>
    let image: MutableProperty<Data?>
    private let store: StoreRepresentable
    private var movie: TMDBMovie
    
    private let (lifetime,token) = Lifetime.make()
    
    // MARK: - Lifecycle
    
    init(movie: TMDBMovie, store: StoreRepresentable) {
        self.store = store
        image = .init(nil)
        dataViewModels = .init([])
        title = .init(value: "")
        self.movie = movie
        load()
    }
    
    public func load(){
        if let path = movie.posterPath {
            store.getImage(imageSize: .big, imagePath: path) { [weak self] in
                switch $0 {
                case .success(let data):
                    self?.image.value = data
                case .failure(let error):
                    print("🚨 : \(error)")
                }
            }
        }
        store.getMovie(movieID: movie.id) { [weak self] in
            switch $0 {
            case .success(let movie):
                guard let strongSelf = self else { return }
                strongSelf.movie = movie
                strongSelf.dataViewModels.value = strongSelf.makeDataViewModels()
            case .failure(let error):
                print("🚨 : \(error)")
            }
        }
    }
    
    // MARK: - Helpers
    
    private func makeDataViewModels() -> [DetailsCellViewModel] {
        var data : [DetailsCellViewModel] = []
        if let overview = movie.overview {
            let overViewViewModel = OverviewViewModel(overview: overview)
            data.append(.overview(overViewViewModel))
        }
        
        let titleViewModel = InfoViewModel(title: "Title",info: movie.title)
        data.append(.info(titleViewModel))
        
        if let status = movie.status {
            let statusViewModel = InfoViewModel(title: "Status", info: status)
            data.append(.info(statusViewModel))
        }
        
        if let budget = movie.budget {
            if budget > 0 {
                let budgetViewModel = InfoViewModel(title: "Budget", info: String(budget))
                data.append(.budget(budgetViewModel))
            }
        }
        
        if let vote = movie.voteAverage {
            let votesViewModel = InfoViewModel(title: "Vote", info: String(vote))
            data.append(.info(votesViewModel))
        }
        return data
    }
}
