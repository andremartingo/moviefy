import Foundation
import ReactiveSwift

class InfoViewModel {
    
    let title: Property<String?>
    let info: Property<String?>
    
    init(title: String, info: String) {
        self.title = .init(value: title)
        self.info = .init(value: info)
    }
}
