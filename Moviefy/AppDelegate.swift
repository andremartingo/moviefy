import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Properties
    
    var window: UIWindow?
    private var coordinator: WindowCoordinator?
    
    // MARK: - Lifecycle
    
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        coordinator = CoordinatorFactory.appCoordinator(window: window!)
        coordinator?.start(animated: false)
        
        return true
    }
}
